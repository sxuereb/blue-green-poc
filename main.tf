module "bootstrap" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/bootstrap.git?ref=v2.1.2"

  bootstrap_version = 9
  teardown_version  = 1
}

provider "google" {
  project = "group-verify-df9383"
  region  = "us-east1"
  zone    = "us-east1-c"
}

data "google_compute_image" "ubuntu-1804" {
  project = "ubuntu-os-cloud"
  family  = "ubuntu-1804-lts"
}

resource "google_compute_instance_template" "runners_manager" {
  name        = "runners-manager-template"
  description = "This template is used to create runner managers instances."

  labels = {
    gl_resource_type     = "ci_manager"
    runner_manager_group = "shared-runners-manager"
  }

  instance_description = "runners managers"
  machine_type         = "e2-medium"

  metadata_startup_script = module.bootstrap.bootstrap

  metadata = {
    "CHEF_URL"     = var.chef_provision["server_url"]
    "CHEF_VERSION" = var.chef_provision["version"]
    //    "GL_KERNEL_VERSION"      = var.kernel_version
    //    "CHEF_ENVIRONMENT"       = var.environment
    //    "CHEF_INIT_RUN_LIST"     = var.chef_init_run_list
    //    "CHEF_RUN_LIST"          = var.chef_run_list
    //    "CHEF_DNS_ZONE_NAME"     = var.dns_zone_name
    //    "CHEF_PROJECT"           = var.project
    "CHEF_BOOTSTRAP_BUCKET"  = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING" = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"     = var.chef_provision["bootstrap_key"]
    //    "block-project-ssh-keys" = var.block_project_ssh_keys
    //    "enable-oslogin"         = var.enable_oslogin
    "shutdown-script" = module.bootstrap.teardown
  }


  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
  }

  disk {
    source_image = data.google_compute_image.ubuntu-1804.self_link
    auto_delete  = true
    boot         = true
  }

  network_interface {
    network = "default"
    access_config {}
  }
}

###############
# Blue        #
###############
resource "google_compute_instance_from_template" "runners_manager_blue" {
  for_each = var.deployment.blue ? var.runner_managers : {}

  name = "runners-manager-${each.key}-blue"
  zone = each.value.zone

  metadata = merge(google_compute_instance_template.runners_manager.metadata, {
    "CHEF_NODE_NAME" = "runners-manager-${each.key}-blue"
  })

  source_instance_template = google_compute_instance_template.runners_manager.id
}

###############
# Green       #
###############
resource "google_compute_instance_from_template" "runners_manager_green" {
  for_each = var.deployment.green ? var.runner_managers : {}

  name = "runners-manager-${each.key}-green"
  zone = each.value.zone

  metadata = merge(google_compute_instance_template.runners_manager.metadata, {
    "CHEF_NODE_NAME" = "runners-manager-${each.key}-green"
  })

  source_instance_template = google_compute_instance_template.runners_manager.id
}
