# Blue Green runner managers deployment PoC

## Covered in this PoC

- Experiment with usage of managed instance groups: https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/13174#note_576561535
- Easy on/off switch for blue/green or both

## Not covered in this PoC

- Chef setup, this might be done in the next iteration.
- Trying to re-architecture the runner static VMs that we have to prevent scope creep.

## Activate blue

1. Inside of [variable.tf](variables.tf) specify the following:

    ```hcl
    variable "deployment" {
      type = map(string)

      default = {
        blue  = true
        green = false
      }
    }
    ```

1. Run `terraform apply`

1. Go inside GCP console
   
    ![blue deployment active in GCP](blue_active.png)

## Activate green

1. Inside of [variable.tf](variables.tf) specify the following:

    ```hcl
    variable "deployment" {
      type = map(string)

      default = {
        blue  = false
        green = true
      }
    }
    ```

1. Run `terraform apply`
1. Go inside GCP console

   ![green deployment active in GCP](green_active.png)


## Activate both

1. Inside of [variable.tf](variables.tf) specify the following:

    ```hcl
    variable "deployment" {
      type = map(string)

      default = {
        blue  = true
        green = true
      }
    }
    ```

1. Run `terraform apply`
1. Go inside GCP console

   ![blue deployment active in GCP](blue_green_active.png)
