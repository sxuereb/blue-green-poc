variable "runner_managers" {
  type = map(object({
    zone = string
  }))

  default = {
    "3" = {
      zone = "us-east1-b"
    }
    "4" = {
      zone = "us-east1-c"
    }
    "5" = {
      zone = "us-east1-d"
    }
    "6" = {
      zone = "us-east1-b"
    }
    "7" = {
      zone = "us-east1-c"
    }
  }
}

variable "deployment" {
  type = map(string)

  default = {
    blue  = true
    green = false
  }
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"

  default = {
    bootstrap_bucket  = "gitlab-ci-chef-bootstrap"
    bootstrap_key     = "gitlab-ci-bootstrap-validation"
    bootstrap_keyring = "gitlab-ci-bootstrap"
    server_url        = "https://api.chef.io/organizations/blue-green-poc"
    user_name         = "gitlab-ci"
    user_key_path     = ".sazzopardi.pem"
    version           = "14.13.11"
  }
}
